# Create a class Employee that will take a full 
# name as argument, as well as a set of none,
# one or more keywords. Each instance should have 
# a name and a lastname attributes plus one more 
# attribute for each of the keywords, if any.
# First and last names will be separated by a whitespace.
# The test will not include any middle names or initials.
# The value of the keywords can be an int, a str or a list.
# Hint: consider using the "setattr" function
# setattr(self, 'best_friend', 'Jimmy')

class Employee:
    def __init__(self, fullname: str, **kwargs):
        self.name, self.lastname = fullname.split(' ')
        for key, value in kwargs.items():
            setattr(self, key, value)

john = Employee("John Doe")
mary = Employee("Mary Major", salary=120000)
richard = Employee("Richard Roe", salary=110000, height=178)
giancarlo = Employee("Giancarlo Rossi", salary=115000, height=182, nationality="Italian")

ans = john.name 
exp = "John"
assert ans == exp, f'Expected {exp}, got {ans}'
ans = mary.lastname 
exp = "Major"
assert ans == exp, f'Expected {exp}, got {ans}'
ans = richard.height 
exp = 178
assert ans == exp, f'Expected {exp}, got {ans}'
ans = giancarlo.nationality 
exp = "Italian"
assert ans == exp, f'Expected {exp}, got {ans}'
print('Nice!')