# "Loves me, loves me not" is a traditional game 
# in which a person plucks off all the petals of 
# a flower one by one, saying the phrase "Loves me" 
# and "Loves me not" when determining whether the 
# one that they love, loves them back.
# Given a number of petals, return a string which 
# repeats the phrases "Loves me" and "Loves me not" 
# for every alternating petal, and return the last 
# phrase in all caps. Remember to put a comma and 
# space between phrases.
# Remember to return a string.
# The first phrase is always "Loves me".

#def loves_me(count: int) -> str:
#    words = []
#    for i in range(count):
#        if i % 2 == 0:
#            words.append('Loves me')
#        else:
#            words.append('Loves me not')
#    words[-1] = words[-1].upper()
#    return ', '.join(words)

def loves_me(count: int) -> str:
    words = ['Loves me not' if i%2 else 'Loves me' for i in range(count)]
    words[-1] = words[-1].upper()
    return ', '.join(words)

ans = loves_me(3)
exp = "Loves me, Loves me not, LOVES ME"
assert ans == exp, f'expected {exp}, got {ans}'
ans = loves_me(6)
exp = "Loves me, Loves me not, Loves me, Loves me not, Loves me, LOVES ME NOT"
assert ans == exp, f'expected {exp}, got {ans}'
ans = loves_me(1)
exp = "LOVES ME"
assert ans == exp, f'expected {exp}, got {ans}'
print('everything is okay')