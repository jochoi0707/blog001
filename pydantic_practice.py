from typing import Optional
from pydantic import BaseModel

class Dog(BaseModel):
    name: str
    age: int
    owner: Optional[str] = 'Steve Jobs'

dog1 = Dog(name='Fido', age=12)

print(dog1)

dog2 = Dog(name='Fifi', age=5, owner='Joseph')
print(dog2)