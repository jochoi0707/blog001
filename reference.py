from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel

class Post(BaseModel):
    pk: Optional[int]
    title: str
    body: str

posts = []
pk = 1


app = FastAPI()

@app.get('/')
async def home():
    return {'message': 'Hello World'}

@app.get('/posts')
async def read_posts():
    return dict(posts=posts)

@app.get('/posts/{post_pk}')
async def read_post(post_pk: int):
    for post in posts:
        if post.pk == post_pk:
            return dict(post=post)
    return dict(post=None)

@app.post('/posts')
async def create_post(post: Post):
    global pk
    post.pk = pk
    pk += 1
    posts.append(post)
    return post

@app.delete('/posts/{post_pk}')
async def delete_post(post_pk: int):
    global posts
    posts = [post for post in posts if post.pk != post_pk]
    return dict(posts=posts)

@app.put('/posts/{post_pk}')
async def update_post(post_pk: int, post: Post):
    for p in posts:
        if p.pk == post_pk:
            p.title = post.title
            p.body = post.body
            return p
    return dict(title=None, body=None)

