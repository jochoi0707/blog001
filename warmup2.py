## Warmup Exercise 2

# The Atbash cipher is an encryption method in 
# which each letter of a word is replaced with 
# its "mirror" letter in the alphabet: A <=> Z; 
# B <=> Y; C <=> X; etc. Create a function that 
# takes a string and applies the Atbash cipher 
# to it. Capitalisation should be retained.
# Non-alphabetic characters should not be altered.
# You may want to use the chr function and an 
# ascii table to save time. Here is an ascii table:
# https://www.asciitable.com/

lower = [chr(n) for n in range(97, 123)]
upper = [chr(n) for n in range(65, 91)]

def atbash(phrase):
    phrase = [c for c in phrase]
    for i, c in enumerate(phrase):
        if c in lower:
            index = lower.index(c)
            mid = len(lower) // 2
            diff = index - mid
            phrase[i] = lower[mid - diff - 1]

        if c in upper:
            index = upper.index(c)
            mid = len(upper) // 2
            diff = index - mid
            phrase[i] = upper[mid - diff - 1]

    return ''.join(phrase)

ans = atbash("apple")
exp = "zkkov"
assert ans == exp, f'Expected {exp}, got {ans}'
ans = atbash("Hello world!")
exp = "Svool dliow!"
assert ans == exp, f'Expected {exp}, got {ans}'
ans = atbash("Christmas is the 25th of December")
exp = "Xsirhgnzh rh gsv 25gs lu Wvxvnyvi"
assert ans == exp, f'Expected {exp}, got {ans}'
print('Nice!')