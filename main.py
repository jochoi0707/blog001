from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional
import csv
from starlette.responses import FileResponse

app = FastAPI()

class BlogPost(BaseModel):
    id: Optional[int]
    title: str
    body: str
    author: Optional[str]

post_id = 0
database = []

try:
    with open('database.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        for post in reader:
            id, title, body, author = post
            id = int(id)
            p = BlogPost(id=id, title=title, body=body, author=author)
            database.append(p)
            post_id = max(post_id, id)
except Exception as e:
    print(e)

@app.get('/')
async def handle_root():
    return FileResponse('index.html')

@app.get('/ping')
async def pong():
    return {
        'message': 'Do you like ping pong?'
    }

@app.get('/repeat-hi/{count}')
async def repeat_hi(count: int):
    message = 'Hi ' * count
    return { 'message': message }

@app.get('/add-nums/{num1}/{num2}')
async def add_nums(num1: int, num2: int):
    sum_ = num1 + num2
    return { 'sum': sum_ }

@app.get('/posts')
async def get_posts():
    '''
    Function for getting full list of all blog posts
    '''
    return {
        'data': database,
        'message': 'Here is the full list of blog posts'
    }

@app.post('/posts')
async def create_post(post: BlogPost):
    '''
    Function for creating a totally new blog post
    '''
    global post_id
    post_id += 1
    post.id = post_id
    database.append(post)

    with open('database.csv', 'a') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"')
        writer.writerow([
            post.id,
            post.title,
            post.body,
            post.author
        ])

    return {
        'data': post,
        'message': 'Blog post was created successfully'
    }

@app.get('/posts/{post_id}')
async def get_post(post_id: int):
    '''
    Function for retrieving a single post
    '''
    data = None
    for post in database:
        if post.id == post_id:
            data = post
            break
    return {
        'data': data,
        'message': 'Here is the blog post you are looking for'
    }

@app.put('/posts/{post_id}')
async def update_post(post_id: int, post: BlogPost):
    '''
    Function for updating a single, existing blog post
    '''
    for item in database:
        if item.id == post_id:
            item.title = post.title
            item.body = post.body
            item.author = post.author
    message = f'Updated post with id: {post_id}'
    return {
        'data': message
    }

@app.delete('/posts/{post_id}')
async def delete_post(post_id: int):
    '''
    Function for deleting a single blog post
    '''
    for i, post in enumerate(database):
        if post.id == post_id:
            database.pop(i)
            break

    message = f'Deleted post with id: {post_id}'
    return {
        'data': message
    }

@app.delete('/posts')
async def delete_all_posts():
    global database
    database = []
    return {
        'data': 'All the posts have been deleted!!'
    }